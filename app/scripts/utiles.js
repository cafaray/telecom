function readImage(input,scope,callback) {
    if ( input.files && input.files[0] ) {
        var FR= new FileReader();
        FR.onload = function(e) {
             //$('#img').attr( "src", e.target.result );
             //$('#base').text( );
             console.info (e.target.result);
             scope.img = e.target.result;
             if(typeof callback !="undefined") callback();             
        };       
        FR.readAsDataURL( input.files[0] );
    }
}
