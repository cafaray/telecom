/* 
 * @Author: yomero
 * @Date:   2014-11-06 15:22:04
 * @Last Modified by:   yomero
 * @Last Modified time: 2014-12-01 16:25:54
 */

/*var urlConsumir = "/notaria/:tipo";

miApp.factory('conexionJSONDATA', function($resource) {
  return $resource(urlConsumir); // Note the full endpoint address
});
*/

/* 
 * @Author: yomero
 * @Date:   2014-11-06 15:22:04
 * @Last Modified by:   yomero
 * @Last Modified time: 2014-11-06 16:02:22
 */

miApp.factory('JSONDATA', function() {

    var JSONDATA = {};

    JSONDATA.filtros =[
    {label:"Concesión",value:"consecion"},
    {label:"Concesionario",value:"consecionario"},
    {label:"Obligacion",value:"nombre"},
    {label:"Estatus",value:"status"},
    {label:"Región",value:"region"},
    {label:"Grupo",value:"grupo"},
    {label:"Servicio",value:"servicio"}];

    JSONDATA.areas = [{
        "id": 1,
        "nombre": "Operaciones",
        "responsable": "Pedro Martinez",
        "email": "pedrom@corporativo.com",
        "notificar_a": ("juanc@corporativo.com,gabrielau@corporativo.com,eduardon@corporativo.com").split(",")
    }, {
        "id": 2,
        "nombre": "Administración",
        "responsable": "Juan Covarrubias",
        "email": "juanc@corporativo.com",
        "notificar_a": ("erikas@corporativo.com,alfredop@corporativo.com,eduardon@corporativo.com").split(",")
    }, {
        "id": 3,
        "nombre": "Finanzas",
        "responsable": "Gabriela Urrutia",
        "email": "gabrielau@corporativo.com",
        "notificar_a": ("juanc@corporativo.com,alfredop@corporativo.com").split(",")
    }, {
        "id": 4,
        "nombre": "Tecnología",
        "responsable": "Eduardo Norzagaray",
        "email": "eduardon@corporativo.com",
        "notificar_a": ("erikas@corporativo.com,gabrielau@corporativo.com,alfredop@corporativo.com").split(",")
    }, {
        "id": 5,
        "nombre": "Logística",
        "responsable": "Alfredo Pacheco",
        "email": "alfredop@corporativo.com",
        "notificar_a": ("pedrom@corporativo.com,juanc@corporativo.com").split(",")
    }, {
        "id": 6,
        "nombre": "Asuntos Jurídicos",
        "responsable": "Erika Saldaña",
        "email": "erikas@corporativo.com",
        "notificar_a": ("alfredop@corporativo.com,eduardon@corporativo.com,juanc@corporativo.com").split(",")
    }];

    JSONDATA.consecionarios = [{
        "id": 1,
        "empresa": "Operbes, S.A. de C.V.",
        "concesiones": [{
            "id": 1
        }, {
            "id": 2
        }],
        "grupo": {
            "id": 1
        },
        "anexo": true
    }, {
        "id": 2,
        "empresa": "Bestphone, S.A. de C.V.",
        "concesiones": [{
            "id": 1
        }, {
            "id": 3
        }],
        "grupo": {
            "id": 1
        },
        "anexo": false
    }, {
        "id": 3,
        "empresa": "Cablemás Telecomunicaciones, S.A. de C.V. (antes Telecable de Chihuahua, S.A. de C.V.)",
        "concesiones": [{
            "id": 3
        }, {
            "id": 2
        }],
        "grupo": {
            "id": 3
        },
        "anexo": true
    }, {
        "id": 4,
        "empresa": "Alvafig, S.A. de C.V.",
        "concesiones": [{
            "id": 1
        }],
        "grupo": {
            "id": 1
        },
        "anexo": false
    }, {
        "id": 5,
        "empresa": "Cablevisión, S.A. de C.V.",
        "concesiones": [{
            "id": 1
        }, {
            "id": 2
        }, {
            "id": 3
        }],
        "grupo": {
            "id": 2
        },
        "anexo": false
    }, {
        "id": 6,
        "empresa": "TVI Nacional, S.A.  de C.V.",
        "concesiones": [{
            "id": 2
        }],
        "grupo": {
            "id": 2
        },
        "anexo": true
    }];

    JSONDATA.conseciones = [{
        "id": 1,
        "nombre": "Tijuana Cable Más",
        "localidad":"Ahumada",
        
    }, {
        "id": 2,
        "nombre": "Mexicali Cablevisión",
        "localidad":"Aldama",
    }, {
        "id": 3,
        "nombre": "Durango Cable más",
        "localidad": "Guerrero",
    }];

    JSONDATA.grupos = [{
        "id": 1,
        "nombre": "Bestel",
        "descripcion": "Info ..."
    }, {
        "id": 2,
        "nombre": "Cablemás",
        "descripcion": "Info ..."
    },{
        "id": 3,
        "nombre": "Cablevisión",
        "descripcion": "Info ..."
    },{
        "id": 4,
        "nombre": "Cablecom",
        "descripcion": "Info ..."
    }];

    JSONDATA.localidades = [{
        "id": 1,
        "nombre": "Ahumada",
        "latitud": "127°234''234'",
        "longitud": "598°325''54'"
    }, {
        "id": 2,
        "nombre": "Aldama",
        "latitud": "454°342''43'",
        "longitud": "987°35''52'"
    }, {
        "id": 3,
        "nombre": "Bocoyna",
        "latitud": "412°543''15'",
        "longitud": "546°144''64'"
    }, {
        "id": 4,
        "nombre": "Guachochi",
        "latitud": "234°623''90'",
        "longitud": "456°355''21'"
    }, {
        "id": 5,
        "nombre": "Guerrero",
        "latitud": "623°246''67'",
        "longitud": "586°34''143'"
    }, {
        "id": 6,
        "nombre": "Jimenez",
        "latitud": "769°65''51'",
        "longitud": "897°235''663'"
    }, {
        "id": 7,
        "nombre": "Madera",
        "latitud": "980°234''64'",
        "longitud": "143°23''45'"
    }, {
        "id": 8,
        "nombre": "Ojinaga",
        "latitud": "23°645''11'",
        "longitud": "13°897''2'"
    }, {
        "id": 9,
        "nombre": "Santa Bárbara",
        "latitud": "987°341''439'",
        "longitud": "234°90''234'"
    }];

    JSONDATA.obligaciones = [{
            "id": 1,
            "nombre": "Contrato con los suscriptores",
            "periodicidad": {
                "id": 1
            },
            "aplica_a": {
                "id": 2
            },
            "descripcion": "N/A",
            "areasInvolucradas": [{
                "id": 5,
                "nombre": "Logística",
                "responsable": "Alfredo Pacheco",
                "email": "alfredop@corporativo.com",
                "notificar_a": "pedrom@corporativo.com,juanc@corporativo.com"
            }, {
                "id": 2,
                "nombre": "Administración",
                "responsable": "Juan Covarrubias",
                "email": "juanc@corporativo.com",
                "notificar_a": "erikas@corporativo.com,alfredop@corporativo.com,eduardon@corporativo.com"
            }, ]
        },

        {
            "id": 2,
            "nombre": "Inicio de operaciones",
            "periodicidad": {
                "id": 6
            },
            "aplica_a": {
                "id": 1
            },
            "descripcion": "N/A",
            "areasInvolucradas": [{
                "id": 3
            }, {
                "id": 1
            }]
        }, {
            "id": 3,
            "nombre": "Aviso Inicio de operaciones",
            "periodicidad": {
                "id": 4
            },
            "aplica_a": {
                "id": 3
            },
            "descripcion": "N/A",
            "areasInvolucradas": [{
                "id": 6
            }, {
                "id": 1
            }, {
                "id": 5
            }]
        }, {
            "id": 4,
            "nombre": "Información estadística",
            "periodicidad": {
                "id": 3
            },
            "aplica_a": {
                "id": 2
            },
            "descripcion": "N/A",
            "areasInvolucradas": [{
                "id": 1
            }]
        }, {
            "id": 5,
            "nombre": "Informe en el que señale las demarcaciones en las que se encuentra en posibilidad de prestar los servicios concesionados",
            "periodicidad": {
                "id": 5
            },
            "aplica_a": {
                "id": 3
            },
            "descripcion": "N/A",
            "areasInvolucradas": [{
                "id": 3
            }, {
                "id": 2
            }]
        }, {
            "id": 6,
            "nombre": "Reporte considerado en los oficios de exención a presuscripción",
            "periodicidad": {
                "id": 2
            },
            "aplica_a": {
                "id": 1
            },
            "descripcion": "N/A",
            "areasInvolucradas": [{
                "id": 6
            }, {
                "id": 4
            }]
        }, {
            "id": 7,
            "nombre": "Informe de operación y calidad de los servicios",
            "periodicidad": {
                "id": 5
            },
            "aplica_a": {
                "id": 2
            },
            "descripcion": "N/A",
            "areasInvolucradas": [{
                "id": 1
            }, {
                "id": 2
            }, {
                "id": 3
            }]
        }
    ];




    JSONDATA.consecionarios = [{
    "id": 1,
    "nombre": "Operbes, S.A. de C.V.",
    "conseciones": [{
        "id": 1,
        "nombre": "Enlaces de microondas punto a multipunto.",
        "porcum": "",
        "obligaciones": [{
            "id": 1,
            "nombre": "Suscripción y enajenación de acciones",
            "fecha": "23-07-2013",
            "fechavencimiento": "23-12-2014",
            "periodicidad": "Trimestral",
            "servicio": "Suscripción y enajenación de acciones",
            "areasinvolucradas": ["Administración"],
            "tipo_obligacion": "L",
            "status": "N",
            "porcum": "",
            "position": 2,
            "historico": [{
                "fecharegistro": "20-12-2013",
                "fechavencimiento": "23-12-2014"
            }, {
                "fecharegistro": "20-10-2013",
                "fechavencimiento": "20-10-2014"
            }, {
                "fecharegistro": "21-08-2013",
                "fechavencimiento": "20-08-2014"
            }, {
                "fecharegistro": "19-06-2013",
                "fechavencimiento": "20-06-2014"
            }]
        }]
    }, {
        "id": 2,
        "nombre": "Enlaces de microondas punto a multipunto.",
        "porcum": "",
        "obligaciones": [{
            "id": 1,
            "nombre": "Designación de responsable técnico",
            "fecha": "26-02-2013",
            "fechavencimiento": "23-12-2014",
            "periodicidad": "A petición",
            "servicio": "Designación de responsable técnico",
            "areasinvolucradas": ["Administración"],
            "tipo_obligacion": "L",
            "status": "N",
            "porcum": "",
            "position": 2,
            "historico": []
        }]
    }, {
        "id": 3,
        "nombre": "Enlaces de microondas punto a multipunto.",
        "porcum": "",
        "obligaciones": [{
            "id": 1,
            "nombre": "Designación de representante legal",
            "fecha": "10-05-2013",
            "fechavencimiento": "23-12-2014",
            "periodicidad": "A petición",
            "servicio": "Designación de representante legal",
            "areasinvolucradas": ["Tecnología"],
            "tipo_obligacion": "L",
            "status": "N",
            "porcum": "",
            "position": 2,
            "historico": []
        }]
    }, {
        "id": 4,
        "nombre": "TV restringida.",
        "porcum": "",
        "obligaciones": [{
            "id": 1,
            "nombre": "Calidad de los servicios (estándares mínimos de calidad)",
            "fecha": "01-08-2013",
            "fechavencimiento": "19-11-2014",
            "periodicidad": "Trimestral",
            "servicio": "Calidad de los servicios (estándares mínimos de calidad)",
            "areasinvolucradas": ["Tecnología"],
            "tipo_obligacion": "L",
            "status": "A",
            "porcum": "",
            "position": 1,
            "historico": [{
                "fecharegistro": "20-01-2013",
                "fechavencimiento": "04-11-2014"
            }, {
                "fecharegistro": "11-03-2013",
                "fechavencimiento": "16-10-2014"
            }, {
                "fecharegistro": "22-06-2013",
                "fechavencimiento": "29-10-2014"
            }, {
                "fecharegistro": "19-05-2013",
                "fechavencimiento": "02-07-2014"
            }]
        }]

    }, {
        "id": 6,
        "nombre": "TV restringida.",
        "porcum": "",
        "obligaciones": [{
            "id": 1,
            "nombre": "Código de prácticas comerciales ",
            "fecha": "15-02-2013",
            "fechavencimiento": "17-11-2014",
            "periodicidad": "Trimestral",
            "servicio": "Código de prácticas comerciales ",
            "areasinvolucradas": ["Administración"],
            "tipo_obligacion": "L",
            "status": "V",
            "porcum": "",
            "position": 0,
            "historico": [{
                "fecharegistro": "18-011-2013",
                "fechavencimiento": "12-11-2014"
            }, {
                "fecharegistro": "11-08-2013",
                "fechavencimiento": "01-11-2014"
            }, {
                "fecharegistro": "02-12-2013",
                "fechavencimiento": "24-09-2014"
            }, {
                "fecharegistro": "30-04-2013",
                "fechavencimiento": "06-06-2014"
            }]
        }]
    }, {
        "id": 5,
        "nombre": "Transmisión Bidireccional de Datos",
        "porcum": "",
        "obligaciones": [{
            "id": 1,
            "nombre": "Calidad de los servicios (estándares mínimos de calidad)",
            "fecha": "01-08-2013",
            "fechavencimiento": "17-11-2014",
            "periodicidad": "",
            "servicio": "Calidad de los servicios (estándares mínimos de calidad)",
            "areasinvolucradas": ["Logística"],
            "tipo_obligacion": "L",
            "status": "V",
            "porcum": "",
            "position": 0,
            "historico": []
        }]
    }, {
        "id": 7,
        "nombre": "Transmisión Bidireccional de Datos",
        "porcum": "",
        "obligaciones": [{
            "id": 1,
            "nombre": "Servicios de emergencia",
            "fecha": "10-07-2013",
            "fechavencimiento": "23-12-2014",
            "periodicidad": "",
            "servicio": "Servicios de emergencia",
            "areasinvolucradas": ["Asuntos Jurídicos"],
            "tipo_obligacion": "L",
            "status": "N",
            "porcum": "",
            "position": 2,
            "historico": []
        }]

    }],
    "porcum": "",
    "grupo": "",
    "region": {
        "nombre": "Guerrero"
    },
    "anexos": ""
},{
    "id": 2,
    "nombre": "Cable y Comunicación de Campeche, S.A. de C.V.",
    "conseciones": [{
        "id": 17,
        "nombre": "Transmisión Bidireccional de Datos",
        "porcum": "",
        "obligaciones": [{
            "id": 1,
            "nombre": "Mapas de cobertura",
            "fecha": "17-09-2013",
            "fechavencimiento": "19-11-2014",
            "periodicidad": "A petición",
            "servicio": "Mapas de cobertura",
            "areasinvolucradas": ["Asuntos Jurídicos"],
            "tipo_obligacion": "C",
            "status": "A",
            "porcum": "",
            "position": 1,
            "historico": []
        }]
    }, {
        "id": 18,
        "nombre": "Enlaces de microondas punto a multipunto.",
        "porcum": "",
        "obligaciones": [{
            "id": 1,
            "nombre": "Información de red y servicios",
            "fecha": "14-10-2013",
            "fechavencimiento": "17-11-2014",
            "periodicidad": "Mensual",
            "servicio": "Información de red y servicios",
            "areasinvolucradas": ["Tecnología"],
            "tipo_obligacion": "C",
            "status": "V",
            "porcum": "",
            "position": 0,
            "historico": [{
                "fecharegistro": "01-12-2013",
                "fechavencimiento": "11-10-2014"
            }, {
                "fecharegistro": "02-06-2013",
                "fechavencimiento": "19-06-2014"
            }, {
                "fecharegistro": "11-11-2013",
                "fechavencimiento": "10-06-2014"
            }, {
                "fecharegistro": "10-02-2013",
                "fechavencimiento": "15-09-2014"
            }, {
                "fecharegistro": "25-02-2013",
                "fechavencimiento": "22-09-2014"
            }, {
                "fecharegistro": "17-01-2013",
                "fechavencimiento": "09-08-2014"
            }, {
                "fecharegistro": "04-05-2013",
                "fechavencimiento": "15-07-2014"
            }, {
                "fecharegistro": "19-07-2013",
                "fechavencimiento": "09-07-2014"
            }, {
                "fecharegistro": "29-01-2013",
                "fechavencimiento": "09-02-2014"
            }, {
                "fecharegistro": "21-09-2013",
                "fechavencimiento": "06-01-2014"
            }, {
                "fecharegistro": "12-12-2013",
                "fechavencimiento": "01-11-2014"
            }, {
                "fecharegistro": "09-10-2013",
                "fechavencimiento": "17-11-2014"
            }]
        }]
    }, {
        "id": 22,
        "nombre": "Enlaces de microondas punto a multipunto.",
        "porcum": "",
        "obligaciones": [{
            "id": 1,
            "nombre": "Modificación de programación o distribución de canales ",
            "fecha": "15-10-2013",
            "fechavencimiento": "19-11-2014",
            "periodicidad": "A petición",
            "servicio": "Modificación de programación o distribución de canales ",
            "areasinvolucradas": ["Administración"],
            "tipo_obligacion": "C",
            "status": "A",
            "porcum": "",
            "position": 1,
            "historico": []
        }]
    }, {
        "id": 19,
        "nombre": "Provisión de capacidad para enlaces de microondas ...",
        "porcum": "",
        "obligaciones": [{
            "id": 1,
            "nombre": "Especificaciones técnicas de la red",
            "fecha": "15-10-2013",
            "fechavencimiento": "23-12-2014",
            "periodicidad": "Unica",
            "servicio": "Especificaciones técnicas de la red",
            "areasinvolucradas": ["Tecnología"],
            "tipo_obligacion": "C",
            "status": "N",
            "porcum": "",
            "position": 2,
            "historico": []
        }]
    }, {
        "id": 20,
        "nombre": "Acceso a Internet",
        "porcum": "",
        "obligaciones": [{
            "id": 1,
            "nombre": "Información de red y servicios",
            "fecha": "14-10-2013",
            "fechavencimiento": "17-11-2014",
            "periodicidad": "Mensual",
            "servicio": "Información de red y servicios",
            "areasinvolucradas": ["Tecnología"],
            "tipo_obligacion": "C",
            "status": "V",
            "porcum": "",
            "position": 0,
            "historico": [{
                "fecharegistro": "01-12-2013",
                "fechavencimiento": "11-10-2014"
            }, {
                "fecharegistro": "02-06-2013",
                "fechavencimiento": "19-06-2014"
            }, {
                "fecharegistro": "11-11-2013",
                "fechavencimiento": "10-06-2014"
            }, {
                "fecharegistro": "10-02-2013",
                "fechavencimiento": "15-09-2014"
            }, {
                "fecharegistro": "25-02-2013",
                "fechavencimiento": "22-09-2014"
            }, {
                "fecharegistro": "17-01-2013",
                "fechavencimiento": "09-08-2014"
            }, {
                "fecharegistro": "04-05-2013",
                "fechavencimiento": "15-07-2014"
            }, {
                "fecharegistro": "19-07-2013",
                "fechavencimiento": "09-07-2014"
            }, {
                "fecharegistro": "29-01-2013",
                "fechavencimiento": "09-02-2014"
            }, {
                "fecharegistro": "21-09-2013",
                "fechavencimiento": "06-01-2014"
            }, {
                "fecharegistro": "12-12-2013",
                "fechavencimiento": "01-11-2014"
            }, {
                "fecharegistro": "09-10-2013",
                "fechavencimiento": "17-11-2014"
            }]
        }]
    }, {
        "id": 21,
        "nombre": "Acceso a Internet",
        "porcum": "",
        "obligaciones": [{
            "id": 1,
            "nombre": "Programación local",
            "fecha": "09-05-2013",
            "fechavencimiento": "17-11-2014",
            "periodicidad": "Mensual",
            "servicio": "Información de red y servicios",
            "areasinvolucradas": ["Logística"],
            "tipo_obligacion": "C",
            "status": "V",
            "porcum": "",
            "position": 0,
            "historico": [{
                "fecharegistro": "23-12-2013",
                "fechavencimiento": "23-10-2014"
            }, {
                "fecharegistro": "12-06-2013",
                "fechavencimiento": "11-06-2014"
            }, {
                "fecharegistro": "10-11-2013",
                "fechavencimiento": "02-06-2014"
            }, {
                "fecharegistro": "10-02-2013",
                "fechavencimiento": "05-09-2014"
            }, {
                "fecharegistro": "18-02-2013",
                "fechavencimiento": "07-09-2014"
            }, {
                "fecharegistro": "01-01-2013",
                "fechavencimiento": "15-08-2014"
            }, {
                "fecharegistro": "04-09-2013",
                "fechavencimiento": "18-07-2014"
            }, {
                "fecharegistro": "09-12-2013",
                "fechavencimiento": "11-07-2014"
            }, {
                "fecharegistro": "12-01-2013",
                "fechavencimiento": "02-02-2014"
            }, {
                "fecharegistro": "21-09-2013",
                "fechavencimiento": "15-01-2014"
            }, {
                "fecharegistro": "12-11-2013",
                "fechavencimiento": "30-11-2014"
            }, {
                "fecharegistro": "09-09-2013",
                "fechavencimiento": "<28-07></28-07>-2014"
            }]
        }]
    }, {
        "id": 23,
        "nombre": "Acceso a Internet",
        "porcum": "",
        "obligaciones": [{
            "id": 1,
            "nombre": "Distribución de las señales de Televisión reservadas al estado",
            "fecha": "01-01-2013",
            "fechavencimiento": "09-01-2015",
            "periodicidad": "Anual",
            "servicio": "Distribución de las señales de Televisión reservadas al estado",
            "areasinvolucradas": ["Tecnología"],
            "tipo_obligacion": "L",
            "status": "N",
            "porcum": "",
            "position": 2,
            "historico": [{
                "fecharegistro": "04-05-2013",
                "fechavencimiento": "12-11-2014"
            }]
        }]
    }, {
        "id": 24,
        "nombre": "Acceso a Internet",
        "porcum": "",
        "obligaciones": [{
            "id": 1,
            "nombre": "Pago de canal 46 (9%)",
            "fecha": "01-01-2013",
            "fechavencimiento": "09-01-2015",
            "periodicidad": "Única",
            "servicio": "Pago de canal 46 (9%)",
            "areasinvolucradas": ["Administración"],
            "tipo_obligacion": "C",
            "status": "A",
            "porcum": "",
            "position": 1,
            "historico": [{
                "fecharegistro": "22-03-2013",
                "fechavencimiento": "02-11-2014"
            }]
        }]

    }], 
    "porcum": "",
    "grupo": "",
    "region": {
        "nombre": "Durango"
    },
    "anexos": ""
}, {
    "id": 3,
    "nombre": "Cablemás Telecomunicaciones, S.A. de C.V.",
    "conseciones": [{
         "id": 8,
        "nombre": "Acceso a Internet",
        "porcum": "",
        "obligaciones": [{
            "id": 1,
            "nombre": "Activos fijos de la empresa",
            "fecha": "23-12-2014",
            "fechavencimiento": "19-11-2014",
            "periodicidad": "Anual",
            "servicio": "Activos fijos de la empresa",
            "areasinvolucradas": ["Asuntos Jurídicos"],
            "tipo_obligacion": "L",
            "status": "A",
            "porcum": "",
            "position": 1,
            "historico": [{
                "fecharegistro": "01-05-2013",
                "fechavencimiento": "12-11-2014"
            }]
        }]
    }, {
        "id": 9,
        "nombre": "Enlaces de microondas punto a multipunto.",
        "porcum": "",
        "obligaciones": [{
            "id": 1,
            "nombre": "Programas de adiestramiento, capacitación del personal y desarrollo tecnológico",
            "fecha": "15-08-2014",
            "fechavencimiento": "19-11-2014",
            "periodicidad": "Trimestral",
            "servicio": "Programas de adiestramiento, capacitación del personal y desarrollo tecnológico",
            "areasinvolucradas": ["Administración"],
            "tipo_obligacion": "L",
            "status": "A",
            "porcum": "",
            "position": 1,
            "historico": [{
                "fecharegistro": "02-05-2013",
                "fechavencimiento": "19-11-2014"
            },{
                "fecharegistro": "16-08-2013",
                "fechavencimiento": "13-06-2014"
            },{
                "fecharegistro": "18-11-2013",
                "fechavencimiento": "09-08-2014"
            },{
                "fecharegistro": "09-12-2013",
                "fechavencimiento": "12-07-2014"
            }]
        }]
    }, {
        "id": 10,
        "nombre": "Provisión de capacidad para enlaces de microondas ...",
        "porcum": "",
        "obligaciones": [{
            "id": 1,
            "nombre": "Información sobre la instalación de la red (expansión de la red)",
            "fecha": "10-04-2013",
            "fechavencimiento": "23-12-2014",
            "periodicidad": "Trimestral",
            "servicio": "Programas de adiestramiento, capacitación del personal y desarrollo tecnológico",
            "areasinvolucradas": ["Administración"],
            "tipo_obligacion": "L",
            "status": "N",
            "porcum": "",
            "position": 2,
            "historico": [{
                "fecharegistro": "02-05-2013",
                "fechavencimiento": "19-11-2014"
            },{
                "fecharegistro": "12-08-2013",
                "fechavencimiento": "13-06-2014"
            },{
                "fecharegistro": "18-11-2013",
                "fechavencimiento": "09-08-2014"
            },{
                "fecharegistro": "09-12-2013",
                "fechavencimiento": "12-07-2014"
            }]
        }]
    }, {
        "id": 11,
        "nombre": "Acceso a Internet",
        "porcum": "",
        "obligaciones": [{
            "id": 1,
            "nombre": "Información Contable",
            "fecha": "10-04-2013",
            "fechavencimiento": "23-12-2014",
            "periodicidad": "Anual",
            "servicio": "Información Contable",
            "areasinvolucradas": ["Asuntos Jurídicos"],
            "tipo_obligacion": "L",
            "status": "V",
            "porcum": "",
            "position": 0,
            "historico": [{
                "fecharegistro": "06-05-2013",
                "fechavencimiento": "19-11-2014"
            }]
        }]
    },{
        "id": 12,
        "nombre": "Acceso a Internet",
        "porcum": "",
        "obligaciones": [{
            "id": 1,
            "nombre": "Manual de separación contable ",
            "fecha": "10-06-2013",
            "fechavencimiento": "19-11-2014",
            "periodicidad": "Anual",
            "servicio": "Manual de separación contable ",
            "areasinvolucradas": ["Finanzas"],
            "tipo_obligacion": "L",
            "status": "A",
            "porcum": "",
            "position": 1,
            "historico": [{
                "fecharegistro": "02-06-2013",
                "fechavencimiento": "14-08-2014"
            }]
        }]
    }, {
        "id": 13,
        "nombre": "Provisión de capacidad para enlaces de microondas ... ",
        "porcum": "",
        "obligaciones": [{
            "id": 1,
            "nombre": "Inscripción del Título de Concesión en el Registro de Telecomunicaciones",
            "fecha": "09-06-2013",
            "fechavencimiento": "23-12-2014",
            "periodicidad": "Trimestral",
            "servicio": "Inscripción del Título de Concesión en el Registro de Telecomunicaciones",
            "areasinvolucradas": ["Finanzas"],
            "tipo_obligacion": "L",
            "status": "N",
            "porcum": "",
            "position": 2,
            "historico": [{
                "fecharegistro": "02-06-2013",
                "fechavencimiento": "15-01-2014"
            },{
                "fecharegistro": "02-06-2013",
                "fechavencimiento": "30-11-2014"
            },{
                "fecharegistro": "02-06-2013",
                "fechavencimiento": "31-10-2014"
            },{
                "fecharegistro": "02-06-2013",
                "fechavencimiento": "18-04-2014"
            }]
        }]
    },{
        "id": 14,
        "nombre": "Enlaces de microondas punto a multipunto.",
        "porcum": "",
        "obligaciones": [{
            "id": 1,
            "nombre": "Informe de ejecución de obras",
            "fecha": "29-07-2013",
            "fechavencimiento": "17-11-2014",
            "periodicidad": "Mensual",
            "servicio": "Informe de ejecución de obras",
            "areasinvolucradas": ["Asuntos Jurídicos"],
            "tipo_obligacion": "L",
            "status": "V",
            "porcum": "",
            "position": 0,
            "historico": [{
                "fecharegistro": "11-10-2013",
                "fechavencimiento": "15-01-2014"
            },{
                "fecharegistro": "09-12-2013",
                "fechavencimiento": "30-11-2014"
            },{
                "fecharegistro": "19-11-2013",
                "fechavencimiento": "31-10-2014"
            },{
                "fecharegistro": "28-01-2013",
                "fechavencimiento": "18-04-2014"
            },{
                "fecharegistro": "24-07-2013",
                "fechavencimiento": "18-04-2014"
            },{
                "fecharegistro": "27-04-2013",
                "fechavencimiento": "08-04-2014"
            },{
                "fecharegistro": "09-09-2013",
                "fechavencimiento": "08-04-2014"
            },{
                "fecharegistro": "02-06-2013",
                "fechavencimiento": "26-04-2014"
            },{
                "fecharegistro": "12-12-2013",
                "fechavencimiento": "16-04-2014"
            },{
                "fecharegistro": "18-09-2013",
                "fechavencimiento": "16-04-2014"
            },{
                "fecharegistro": "02-12-2013",
                "fechavencimiento": "08-04-2014"
            },{
                "fecharegistro": "07-07-2013",
                "fechavencimiento": "17-04-2014"
            }]
        }]
    }, {
        "id": 15,
        "nombre": "Acceso a Internet",
        "porcum": "",
        "obligaciones": [{
            "id": 1,
            "nombre": "Cobertura y conectividad social y rural",
            "fecha": "19-03-2013",
            "fechavencimiento": "19-11-2014",
            "periodicidad": "Trimestral",
            "servicio": "Cobertura y conectividad social y rural",
            "areasinvolucradas": ["Administración"],
            "tipo_obligacion": "L",
            "status": "A",
            "porcum": "",
            "position": 1,
            "historico": [{
                "fecharegistro": "11-10-2013",
                "fechavencimiento": "21-09-2014"
            },{
                "fecharegistro": "09-12-2013",
                "fechavencimiento": "22-10-2014"
            },{
                "fecharegistro": "19-11-2013",
                "fechavencimiento": "30-11-2014"
            },{
                "fecharegistro": "28-01-2013",
                "fechavencimiento": "02-06-2014"
            }]
        }]
    }, {
        "id": 16,
        "nombre": "TV restringida",
        "porcum": "",
        "obligaciones": [{
            "id": 1,
            "nombre": "Modernización de la red",
            "fecha": "11-03-2013",
            "fechavencimiento": "23-12-2014",
            "periodicidad": "A petición",
            "servicio": "Modernización de la red",
            "areasinvolucradas": ["Administración"],
            "tipo_obligacion": "L",
            "status": "N",
            "porcum": "",
            "position": 2,
            "historico": [{
                "fecharegistro": "02-10-2013",
                "fechavencimiento": "28-05-2014"
            }]
        }]
    }],
    "porcum": "",
    "grupo": "",
    "region": {
        "nombre": "Pachuca"
    },
    "anexos": ""
},{
    "id": 4,
    "nombre": "Tele Azteca, S.A. de C.V.",
    "conseciones": [{
        "id": 25,
        "nombre": "Provisión de capacidad para enlaces de microondas ... ",
        "porcum": "",
        "obligaciones": [{
            "id": 1,
            "nombre": "Inicio de operaciones",
            "fecha": "29-08-2013",
            "fechavencimiento": "17-11-2014",
            "periodicidad": "Unica",
            "servicio": "Inicio de operaciones",
            "areasinvolucradas": ["Asuntos Jurídicos"],
            "tipo_obligacion": "C",
            "status": "V",
            "porcum": "",
            "position": 0,
            "historico": [{
                "fecharegistro": "18-02-2013",
                "fechavencimiento": "16-11-2014"
            }]
        }]
    },{
        "id": 26,
        "nombre": "Enlaces de microondas punto a multipunto.",
        "porcum": "",
        "obligaciones": [{
            "id": 1,
            "nombre": "Aviso Inicio de operaciones",
            "fecha": "07-09-2013",
            "fechavencimiento": "12-10-2014",
            "periodicidad": "Unica",
            "servicio": "Aviso Inicio de operaciones",
            "areasinvolucradas": ["Tecnología"],
            "tipo_obligacion": "C",
            "status": "N",
            "porcum": "",
            "position": 2,
            "historico": [{
                "fecharegistro": "05-03-2013",
                "fechavencimiento": "18-11-2014"
            }]
        }]
    },{
        "id": 27,
        "nombre": "Acceso a Internet",
        "porcum": "",
        "obligaciones": [{
            "id": 1,
            "nombre": "Regla 38 de las Reglas del Servicio de Larga Distancia",
            "fecha": "01-02-2013",
            "fechavencimiento": "11-11-2014",
            "periodicidad": "Trimestral",
            "servicio": "Regla 38 de las Reglas del Servicio de Larga Distancia",
            "areasinvolucradas": ["Asuntos Jurídicos"],
            "tipo_obligacion": "C",
            "status": "A",
            "porcum": "",
            "position": 1,
            "historico": [{
                "fecharegistro": "02-02-2013",
                "fechavencimiento": "18-11-2014"
            },{
                "fecharegistro": "10-02-2013",
                "fechavencimiento": "29-11-2014"
            },{
                "fecharegistro": "16-12-2013",
                "fechavencimiento": "26-11-2014"
            },{
                "fecharegistro": "09-12-2013",
                "fechavencimiento": "19-11-2014"
            }]
        }]
    },{
        "id": 28,
        "nombre": "Acceso a Internet",
        "porcum": "",
        "obligaciones": [{
            "id": 1,
            "nombre": "Regla 39 de las Reglas del Servicio de Larga Distancia",
            "fecha": "01-02-2013",
            "fechavencimiento": "11-11-2014",
            "periodicidad": "Trimestral",
            "servicio": "Regla 39 de las Reglas del Servicio de Larga Distancia",
            "areasinvolucradas": ["Administración"],
            "tipo_obligacion": "C",
            "status": "V",
            "porcum": "",
            "position": 0,
            "historico": [{
                "fecharegistro": "01-02-2013",
                "fechavencimiento": "18-11-2014"
            },{
                "fecharegistro": "18-02-2013",
                "fechavencimiento": "19-11-2014"
            },{
                "fecharegistro": "12-06-2013",
                "fechavencimiento": "26-11-2014"
            },{
                "fecharegistro": "28-11-2013",
                "fechavencimiento": "17-11-2014"
            }]
        }]
    },{
        "id": 29,
        "nombre": "Acceso a Internet",
        "porcum": "",
        "obligaciones": [{
            "id": 1,
            "nombre": "Regla 42 de las Reglas del Servicio Local",
            "fecha": "18-12-2013",
            "fechavencimiento": "19-01-2015",
            "periodicidad": "Trimestral",
            "servicio": "Regla 42 de las Reglas del Servicio Local",
            "areasinvolucradas": ["Finanzas"],
            "tipo_obligacion": "L",
            "status": "N",
            "porcum": "",
            "position": 2,
            "historico": [{
                "fecharegistro": "03-02-2013",
                "fechavencimiento": "18-04-2014"
            },{
                "fecharegistro": "01-02-2013",
                "fechavencimiento": "19-08-2014"
            },{
                "fecharegistro": "12-06-2013",
                "fechavencimiento": "26-10-2014"
            },{
                "fecharegistro": "28-11-2013",
                "fechavencimiento": "17-10-2014"
            }]
        }]
    },{
        "id": 30,
        "nombre": "Acceso a Internet",
        "porcum": "",
        "obligaciones": [{
            "id": 1,
            "nombre": "Regla 42 de las Reglas del Servicio Local",
            "fecha": "18-12-2013",
            "fechavencimiento": "19-11-2014",
            "periodicidad": "Anual",
            "servicio": "Regla 42 de las Reglas del Servicio Local",
            "areasinvolucradas": ["Asuntos Jurídicos"],
            "tipo_obligacion": "L",
            "status": "A",
            "porcum": "",
            "position": 1,
            "historico": [{
                "fecharegistro": "11-07-2013",
                "fechavencimiento": "18-09-2014"
            }]
        }]
    },{
        "id": 31,
        "nombre": "Provisión de capacidad para enlaces de microondas ... ",
        "porcum": "",
        "obligaciones": [{
            "id": 1,
            "nombre": "Informe de operación y calidad de los servicios",
            "fecha": "18-12-2013",
            "fechavencimiento": "19-11-2014",
            "periodicidad": "Trimestral",
            "servicio": "Informe de operación y calidad de los servicios",
            "areasinvolucradas": ["Logística"],
            "tipo_obligacion": "L",
            "status": "A",
            "porcum": "",
            "position": 1,
            "historico": [{
                "fecharegistro": "16-07-2013",
                "fechavencimiento": "18-04-2014"
            },{
                "fecharegistro": "13-07-2013",
                "fechavencimiento": "18-08-2014"
            },{
                "fecharegistro": "12-07-2013",
                "fechavencimiento": "18-05-2014"
            },{
                "fecharegistro": "11-09-2013",
                "fechavencimiento": "18-09-2014"
            }]
        }]
    },{
        "id": 32,
        "nombre": "Enlaces de microondas punto a multipunto.",
        "porcum": "",
        "obligaciones": [{
            "id": 1,
            "nombre": "Informe de operación y calidad de los servicios",
            "fecha": "15-11-2013",
            "fechavencimiento": "23-12-2014",
            "periodicidad": "Trimestral",
            "servicio": "Informe de operación y calidad de los servicios",
            "areasinvolucradas": ["Logística"],
            "tipo_obligacion": "L",
            "status": "N",
            "porcum": "",
            "position": 2,
            "historico": [{
                "fecharegistro": "16-07-2013",
                "fechavencimiento": "18-04-2014"
            }]
        }]
    },{
        "id": 33,
        "nombre": "Acceso a Internet",
        "porcum": "",
        "obligaciones": [{
            "id": 1,
            "nombre": "Sistema de facturación",
            "fecha": "15-11-2013",
            "fechavencimiento": "23-12-2014",
            "periodicidad": "Trimestral",
            "servicio": "Sistema de facturación",
            "areasinvolucradas": ["Administración"],
            "tipo_obligacion": "L",
            "status": "A",
            "porcum": "",
            "position": 1,
            "historico": [{
                "fecharegistro": "06-07-2013",
                "fechavencimiento": "06-08-2014"
            },{
                "fecharegistro": "26-05-2013",
                "fechavencimiento": "23-06-2014"
            },{
                "fecharegistro": "16-03-2013",
                "fechavencimiento": "16-01-2014"
            },{
                "fecharegistro": "11-07-2013",
                "fechavencimiento": "12-04-2014"
            }]
        }]
    },{
        "id": 34,
        "nombre": "Acceso a Internet",
        "porcum": "",
        "obligaciones": [{
            "id": 1,
            "nombre": "Sistema de quejas y reparación de fallas",
            "fecha": "15-11-2013",
            "fechavencimiento": "23-12-2014",
            "periodicidad": "Trimestral",
            "servicio": "Sistema de quejas y reparación de fallas",
            "areasinvolucradas": ["Tecnología"],
            "tipo_obligacion": "L",
            "status": "A",
            "porcum": "",
            "position": 1,
            "historico": [{
                "fecharegistro": "06-07-2013",
                "fechavencimiento": "06-08-2014"
            },{
                "fecharegistro": "26-05-2013",
                "fechavencimiento": "23-06-2014"
            },{
                "fecharegistro": "16-03-2013",
                "fechavencimiento": "16-01-2014"
            },{
                "fecharegistro": "11-07-2013",
                "fechavencimiento": "12-04-2014"
            }]
        }]
    },{
        "id": 35,
        "nombre": "Acceso a Internet",
        "porcum": "",
        "obligaciones": [{
            "id": 1,
            "nombre": "Inscripción en el Registro de Telecomunicaciones del anexo ...",
            "fecha": "15-11-2013",
            "fechavencimiento": "23-12-2014",
            "periodicidad": "Única",
            "servicio": "Inscripción en el Registro de Telecomunicaciones del anexo ...",
            "areasinvolucradas": ["Tecnología"],
            "tipo_obligacion": "L",
            "status": "A",
            "porcum": "",
            "position": 1,
            "historico": [{
                "fecharegistro": "023-07-2013",
                "fechavencimiento": "05-09-2014"
            }]
        }]
    },{
        "id": 36,
        "nombre": "Provisión de capacidad para enlaces de microondas ... ",
        "porcum": "",
        "obligaciones": [{
            "id": 1,
            "nombre": "Reportes de trafico de los concesionarios de Larga Distancia",
            "fecha": "15-11-2013",
            "fechavencimiento": "17-11-2014",
            "periodicidad": "Mensual",
            "servicio": "Reportes de trafico de los concesionarios de Larga Distancia",
            "areasinvolucradas": ["Administración"],
            "tipo_obligacion": "L",
            "status": "V",
            "porcum": "",
            "position": 0,
            "historico": [{
                "fecharegistro": "11-10-2013",
                "fechavencimiento": "15-01-2014"
            },{
                "fecharegistro": "09-12-2013",
                "fechavencimiento": "30-11-2014"
            },{
                "fecharegistro": "19-11-2013",
                "fechavencimiento": "31-10-2014"
            },{
                "fecharegistro": "28-01-2013",
                "fechavencimiento": "18-04-2014"
            },{
                "fecharegistro": "24-07-2013",
                "fechavencimiento": "18-04-2014"
            },{
                "fecharegistro": "27-04-2013",
                "fechavencimiento": "08-04-2014"
            },{
                "fecharegistro": "09-09-2013",
                "fechavencimiento": "08-04-2014"
            },{
                "fecharegistro": "02-06-2013",
                "fechavencimiento": "26-04-2014"
            },{
                "fecharegistro": "12-12-2013",
                "fechavencimiento": "16-04-2014"
            },{
                "fecharegistro": "18-09-2013",
                "fechavencimiento": "16-04-2014"
            },{
                "fecharegistro": "02-12-2013",
                "fechavencimiento": "08-04-2014"
            },{
                "fecharegistro": "07-07-2013",
                "fechavencimiento": "17-04-2014"
            }]
        }]
    },{
        "id": 37,
        "nombre": "Enlaces de microondas punto a multipunto.",
        "porcum": "",
        "obligaciones": [{
            "id": 1,
            "nombre": "Regla 23 de las Reglas de Telecomunicaciones Internacionales",
            "fecha": "15-11-2013",
            "fechavencimiento": "17-11-2014",
            "periodicidad": "Mensual",
            "servicio": "Regla 23 de las Reglas de Telecomunicaciones Internacionales",
            "areasinvolucradas": ["Tecnología"],
            "tipo_obligacion": "L",
            "status": "N",
            "porcum": "",
            "position": 2,
            "historico": [{
                "fecharegistro": "09-12-2013",
                "fechavencimiento": "30-11-2014"
            },{
                "fecharegistro": "24-07-2013",
                "fechavencimiento": "18-04-2014"
            },{
                "fecharegistro": "12-12-2013",
                "fechavencimiento": "16-04-2014"
            },{
                "fecharegistro": "07-07-2013",
                "fechavencimiento": "17-04-2014"
            }]
        }]
    }],
    "porcum": "",
    "grupo": "",
    "region": {
        "nombre": "Oaxaca"
    },
    "anexos": ""
}];



    JSONDATA.getObligacionesParsed = function() {
    JSONDATA.obligacionesParsed = [];
        _.each(JSONDATA.consecionarios, function(item, index) {
            var tmp_consecionario = {};
            tmp_consecionario.nombre = item.nombre;
            tmp_consecionario.id = item.id;
            _.each(item.conseciones, function(consecion) {
                var tmp_consecion = {};
                tmp_consecion.id = consecion.id;
                tmp_consecion.nombre = consecion.nombre;
                _.each(consecion.obligaciones, function(obligacion) {
                    obligacion.consecionario = tmp_consecionario;
                    obligacion.consecion = tmp_consecion;
                    obligacion.region = item.region;
                    obligacion.view = false;
                    var tmpfecha = obligacion.fechavencimiento.split("-");
                    obligacion.tmpfecha = tmpfecha[2] + tmpfecha[1] + tmpfecha[0];
                    if (obligacion.status == "V") {
                        obligacion.view = true;
                    }
                    JSONDATA.obligacionesParsed.push(obligacion);
                })

            })
        })


        return JSONDATA.obligacionesParsed;

    }


    
    JSONDATA.actualizaobligacion = [{
        "id": 1,
        "nombre": "Suscripción y enajenación de acciones",
        "fechavencimiento": "23-12-2014",
        "status": "N",
        "position": 2,
        "consecionario": "Operbes, S.A. de C.V.",
        "areasinvolucradas": ["Administración"],
        "concesion": "Enlaces de microondas punto a multipunto",
        "grupo": "",
        "region": "Guerrero"
    }, {
        "id": 2,
        "nombre": "Designación de responsable técnico",
        "fechavencimiento": "23-12-2014",
        "position": 2,
        "status": "N",
        "consecionario": "Operbes, S.A. de C.V.",
        "areasinvolucradas": ["Administración"],
        "concesion": "TV restringida",
        "grupo": "",
        "region": "Guerrero"

    }, {
        "id": 3,
        "nombre": "Designación de representante legal",
        "fechavencimiento": "23-12-2014",
        "status": "N",
        "position": 2,
        "consecionario": "Operbes, S.A. de C.V.",
        "areasinvolucradas": ["Tecnología"],
        "concesion": "Transmision bidireccion de datos",
        "grupo": "",
        "region": "Pachuca"
    }, {
        "id": 4,
        "nombre": "Calidad de los servicios (estándares mínimos de calidad)",
        "fechavencimiento": "19-11-2014",
        "status": "A",
        "position": 1,
        "consecionario": "Operbes, S.A. de C.V.",
        "areasinvolucradas": ["Tecnología"],
        "concesion": "Transmision bidirecional de datos",
        "grupo": "",
        "region": "Guerrero"
    },  {
        "id": 5,
        "nombre": "Sistema de quejas y reparación de fallas",
        "fechavencimiento": "17-11-2014",
        "status": "V",
        "position": 0,
        "consecionario": "Operbes, S.A. de C.V.",
        "areasinvolucradas": ["Logística"],
        "concesion": "Enlaces de microondas punto a multipunto",
        "grupo": "",
        "region": "Oaxaca"
    }, {
        "id": 6,
        "nombre": "Código de prácticas comerciales",
        "fechavencimiento": "17-11-2014",
        "status": "V",
        "position": 0,
        "consecionario": "Operbes, S.A. de C.V.",
        "areasinvolucradas": ["Administración"],
        "concesion": "TV restringida",
        "grupo": "",
        "region": "Durango"
    }, {
        "id": 7,
        "nombre": "Servicios de emergencia",
        "fechavencimiento": "23-12-2014",
        "status": "N",
        "position": 2,
        "consecionario": "Operbes, S.A. de C.V.",
        "areasinvolucradas": ["Asuntos Jurídicos"],
        "concesion": "TV restringida",
        "grupo": "",
        "region": "Pachuca"
    }, {
        "id": 8,
        "nombre": "Activos fijos de la empresa",
        "fechavencimiento": "19-11-2014",
        "status": "A",
        "position": 1,
        "consecionario": "Cablemás Telecomunicaciones, S.A. de C.V.",
        "areasinvolucradas": ["Asuntos Jurídicos"],
        "concesion": "Enlaces de microondas punto a multipunto",
        "grupo": "",
        "region": "Guerrero"
    }, {
        "id": 9,
        "nombre": "Programas de adiestramiento, capacitación del personal y desarrollo tecnológico",
        "fechavencimiento": "19-11-2014",
        "status": "A",
        "position": 1,
        "consecionario": "Cablemás Telecomunicaciones, S.A. de C.V.",
        "areasinvolucradas": ["Administración"],
        "concesion": "Provicion de capacidad para enlaces de microondas punto a punto",
        "grupo": "",
        "region": "Oaxaca"
    }, {
        "id": 10,
        "nombre": "Información sobre la instalación de la red (expansión de la red)",
        "fechavencimiento": "23-12-2014",
        "status": "N",
        "position": 2,
        "consecionario": "Cablemás Telecomunicaciones, S.A. de C.V.",
        "areasinvolucradas": ["Finanzas"],
        "concesion": "Acceso a internet",
        "grupo": "",
        "region": "Pachuca"
    }, {
        "id": 11,
        "nombre": "Información Contable",
        "fechavencimiento": "17-11-2014",
        "status": "V",
        "position": 0,
        "consecionario": "Cablemás Telecomunicaciones, S.A. de C.V.",
        "areasinvolucradas": ["Asuntos Jurídicos"],
        "concesion": "TV restringida",
        "grupo": "",
        "region": "Durango"
    }, {
        "id": 12,
        "nombre": "Manual de separación contable",
        "fechavencimiento": "19-11-2014",
        "status": "A",
        "position": 1,
        "consecionario": "Cablemás Telecomunicaciones, S.A. de C.V.",
        "areasinvolucradas": ["Finanzas"],
        "concesion": "Provicion de capacidad de enlaces de microondas punto a punto",
        "grupo": "",
        "region": "Durango"
    }, {
        "id": 13,
        "nombre": "Inscripción del Título de Concesión en el Registro de Telecomunicaciones",
        "fechavencimiento": "23-12-2014",
        "status": "N",
        "position": 2,
        "consecionario": "Cablemás Telecomunicaciones, S.A. de C.V.",
        "areasinvolucradas": ["Asuntos Jurídicos"],
        "concesion": "Acceso a internet",
        "grupo": "",
        "region": "Pachuca"
    }, {
        "id": 14,
        "nombre": "Informe de ejecución de obras",
        "fechavencimiento": "17-11-2014",
        "status": "V",
        "position": 0,
        "consecionario": "Cablemás Telecomunicaciones, S.A. de C.V.",
        "areasinvolucradas": ["Asuntos Jurídicos"],
        "concesion": "Enlaces de microondas punto a multipunto",
        "grupo": "",
        "region": "Guerrero"
    }, {
        "id": 15,
        "nombre": "Cobertura y conectividad social y rural",
        "fechavencimiento": "19-11-2014",
        "status": "A",
        "position": 1,
        "consecionario": "Cablemás Telecomunicaciones, S.A. de C.V.",
        "areasinvolucradas": ["Administración"],
        "concesion": "TV restringida",
        "grupo": "",
        "region": "Oaxaca"
    }, {
        "id": 16,
        "nombre": "Modernización de la red",
        "fechavencimiento": "23-12-2014",
        "status": "N",
        "position": 2,
        "consecionario": "Cablemás Telecomunicaciones, S.A. de C.V.",
        "areasinvolucradas": ["Administración"],
        "concesion": "Provicion de capacidad para enlaces de microondas punto a punto",
        "grupo": "",
        "region": "Pachuca"
    }, {
        "id": 17,
        "nombre": "Mapas de cobertura",
        "fechavencimiento": "19-11-2014",
        "status": "A",
        "position": 1,
        "consecionario": "Cable y Comunicación de Campeche, S.A. de C.V.",
        "areasinvolucradas": ["Asuntos Jurídicos"],
        "concesion": "Transmision bidireccional de datos",
        "grupo": "",
        "region": "Guerrero"
    }, {
        "id": 18,
        "nombre": "Información de red y servicios",
        "fechavencimiento": "17-11-2014",
        "status": "V",
        "position": 0,
        "consecionario": "Cable y Comunicación de Campeche, S.A. de C.V.",
        "areasinvolucradas": ["Tecnología"],
        "concesion": "Acceso a internet",
        "grupo": "",
        "region": "Oaxaca"
    }, {
        "id": 19,
        "nombre": "Especificaciones técnicas de la red",
        "fechavencimiento": "23-12-2014",
        "status": "N",
        "position": 2,
        "consecionario": "Cable y Comunicación de Campeche, S.A. de C.V.",
        "areasinvolucradas": ["Tecnologia"],
        "concesion": "Enlaces de microondas punto a multipunto",
        "grupo": "",
        "region": "Oaxaca"
    }, {
        "id": 20,
        "nombre": "Reporte de operación del servicio",
        "fechavencimiento": "19-11-2014",
        "status": "A",
        "position": 1,
        "consecionario": "Cable y Comunicación de Campeche, S.A. de C.V.",
        "areasinvolucradas": ["Finanzas"],
        "concesion": "Transmision bidireccional de datos",
        "grupo": "",
        "region": "Guerrero"
    }, {
        "id": 21,
        "nombre": "Programación local",
        "fechavencimiento": "17-11-2014",
        "status": "V",
        "position": 0,
        "consecionario": "Cable y Comunicación de Campeche, S.A. de C.V.",
        "areasinvolucradas": ["Logística"],
        "concesion": "Provicion de capacidad de enlaces de microondas punto a punto",
        "grupo": "",
        "region": "Pachuca"
    }, {
        "id": 22,
        "nombre": "Modificación de programación o distribución de canales",
        "fechavencimiento": "19-11-2014",
        "status": "A",
        "position": 1,
        "consecionario": "Cable y Comunicación de Campeche, S.A. de C.V.",
        "areasinvolucradas": ["Administración"],
        "concesion": "TV restringida",
        "grupo": "",
        "region": "Oaxaca"
    }, {
        "id": 23,
        "nombre": "Distribución de las señales de Televisión reservadas al estado",
        "fechavencimiento": "09-01-2015",
        "status": "N",
        "position": 2,
        "consecionario": "Cable y Comunicación de Campeche, S.A. de C.V.",
        "areasinvolucradas": ["Tecnología"],
        "concesion": "Enlaces de microondas punto a multipunto",
        "grupo": "",
        "region": "Oaxaca"
    }, {
        "id": 24,
        "nombre": "Pago de canal 46 (9%)",
        "fechavencimiento": "19-11-2014",
        "status": "A",
        "position": 1,
        "consecionario": "Cable y Comunicación de Campeche, S.A. de C.V.",
        "areasinvolucradas": ["Administración"],
        "concesion": "Transmision bidireccional de datos",
        "grupo": "",
        "region": "Guerrero"
    }, {
        "id": 25,
        "nombre": "Inicio de operaciones",
        "fechavencimiento": "17-11-2014",
        "status": "V",
        "position": 0,
        "consecionario": "Tele Azteca, S.A. de C.V.",
        "areasinvolucradas": ["Asuntos Jurídicos"],
        "concesion": "Provicion de capacidad para enlaces de microondas punto a punto",
        "grupo": "",
        "region": "Oaxaca"
    }, {
        "id": 26,
        "nombre": "Aviso Inicio de operaciones",
        "fechavencimiento": "23-12-2014",
        "status": "N",
        "position": 2,
        "consecionario": "Tele Azteca, S.A. de C.V.",
        "areasinvolucradas": ["Tecnología"],
        "concesion": "Acceso a internet",
        "grupo": "",
        "region": "Pachuca"
    }, {
        "id": 27,
        "nombre": "Regla 38 de las Reglas del Servicio de Larga Distancia",
        "fechavencimiento": "19-11-2014",
        "status": "A",
        "position": 1,
        "consecionario": "Tele Azteca, S.A. de C.V.",
        "areasinvolucradas": ["Asuntos Jurídicos"],
        "concesion": "Enlaces de microondas punto a multipunto",
        "grupo": "",
        "region": "Oaxaca"
    }, {
        "id": 28,
        "nombre": "Regla 39 de las Reglas del Servicio de Larga Distancia",
        "fechavencimiento": "17-11-2014",
        "status": "V",
        "position": 0,
        "consecionario": "Tele Azteca, S.A. de C.V.",
        "areasinvolucradas": ["Administración"],
        "concesion": "Acceso a internet",
        "grupo": "",
        "region": "Guerrero"
    }, {
        "id": 29,
        "nombre": "Regla 42 de las Reglas del Servicio Local",
        "fechavencimiento": "09-01-2015",
        "status": "N",
        "position": 2,
        "consecionario": "Tele Azteca, S.A. de C.V.",
        "areasinvolucradas": ["Finanzas"],
        "concesion": "Provicion de capacidad para enlaces de microondas punto a punto",
        "grupo": "",
        "region": "Durango"
    }, {
        "id": 30,
        "nombre": "Información estadística",
        "fechavencimiento": "19-11-2014",
        "status": "A",
        "position": 1,
        "consecionario": "Tele Azteca, S.A. de C.V.",
        "areasinvolucradas": ["Asuntos Jurídicos"],
        "concesion": "Acceso a internet",
        "grupo": "",
        "region": "Oaxaca"
    }, {
        "id": 31,
        "nombre": "Informe de operación y calidad de los servicios",
        "fechavencimiento": "17-11-2014",
        "status": "V",
        "position": 0,
        "consecionario": "Tele Azteca, S.A. de C.V.",
        "areasinvolucradas": ["Logística"],
        "concesion": "Enlaces de microondas punto a multipunto",
        "grupo": "",
        "region": "Durango"
    }, {
        "id": 32,
        "nombre": "Actualización del Código de Practicas Comerciales",
        "fechavencimiento": "23-12-2014",
        "status": "N",
        "position": 2,
        "consecionario": "Tele Azteca, S.A. de C.V.",
        "areasinvolucradas": ["Logística"],
        "concesion": "Provicion de capacidad para enlaces de microondas punto a punto",
        "grupo": "",
        "region": "Pachuca"
    }, {
        "id": 33,
        "nombre": "Sistema de facturación",
        "fechavencimiento": "19-11-2014",
        "status": "A",
        "position": 1,
        "consecionario": "Tele Azteca, S.A. de C.V.",
        "areasinvolucradas": ["Administración"],
        "concesion": "Acceso a internet",
        "grupo": "",
        "region": "Guerrero"
    }, {
        "id": 34,
        "nombre": "Inscripción en el Registro de Telecomunicaciones del anexo ...",
        "fechavencimiento": "19-11-2014",
        "status": "A",
        "position": 1,
        "consecionario": "Tele Azteca, S.A. de C.V.",
        "areasinvolucradas": ["Tecnología"],
        "concesion": "Enlaces de microondas punto a multipunto",
        "grupo": "",
        "region": "Pachuca"
    }, {
        "id": 35,
        "nombre": "Inscripción en el Registro de Telecomunicaciones del anexo ...",
        "fechavencimiento": "19-11-2014",
        "status": "A",
        "position": 1,
        "consecionario": "Tele Azteca, S.A. de C.V.",
        "areasinvolucradas": ["Tecnología"],
        "concesion": "Enlaces de microondas punto a multipunto",
        "grupo": "",
        "region": "Oaxaca"
    }, {
        "id": 36,
        "nombre": "Reportes de trafico de los concesionarios de Larga Distancia",
        "fechavencimiento": "17-11-2014",
        "status": "V",
        "position": 0,
        "consecionario": "Tele Azteca, S.A. de C.V.",
        "areasinvolucradas": ["Administración"],
        "concesion": "Provicion de capacidad para enlaces de microondas punto a punto",
        "grupo": "",
        "region": "Guerrero"
    }, {
        "id": 37,
        "nombre": "Regla 23 de las Reglas de Telecomunicaciones Internacionales",
        "fechavencimiento": "23-12-2014",
        "status": "N",
        "position": 2,
        "consecionario": "Tele Azteca, S.A. de C.V.",
        "areasinvolucradas": ["Tecnología"],
        "concesion": "Acceso a internet",
        "grupo": "",
        "region": "Durango"
    }
    ];



    JSONDATA.periodicidades = [{
        "id": 1,
        "nombre": "Mensual"
    }, {
        "id": 2,
        "nombre": "Trimestral"
    }, {
        "id": 3,
        "nombre": "Semestral"
    }, {
        "id": 4,
        "nombre": "Anual"
    }, {
        "id": 5,
        "nombre": "Única"
    }, {
        "id": 6,
        "nombre": "Por requerimiento"
    }, {
        "id": 6,
        "nombre": "Última"
    }];

    JSONDATA.aplica_a = [{
        "id": 1,
        "nombre": "Enlaces de microondas punto a multipunto"
    }, {
        "id": 2,
        "nombre": "Provisión de capacidad para enlaces de microondas punto a punto"
    }, {
        "id": 3,
        "nombre": "Telefonía LD Nacional e Internacional, Internet"
    }];



    return JSONDATA; 
});