'use strict';

/**
 * @ngdoc overview
 * @name televisaApp
 * @description
 * # televisaApp
 *
 * Main module of the application.
 */

var miApp = angular.module('televisaApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ]).config(function ($routeProvider) {
    $routeProvider
      .when('/obligaciones', {
        templateUrl: 'views/obligaciones.html',
        controller: 'ctrl_obligaciones'
      })
      .when('/reg_obligacion', {
        templateUrl: 'views/reg_obligacion.html',
        controller: 'ctrl_generic_datos'
      }).when('/actualizacion_obligaciones', {
        templateUrl: 'views/actu_obligacion.html',
        controller: ''
      }).when('/actualizacion_obligaciones2', {
        templateUrl: 'views/actu_obligacion2.html',
        controller: 'ctrl_obligaciones2'
      })
      .when('/resultados', {
        templateUrl: 'views/resultados.html',
        controller: 'ctrl_resultados'
      })
      .when('/informes', {
        templateUrl: 'views/gen_informes.html',
        controller: 'ctrl_informes'
      })
      .when('/porcentaje_cumplimiento', {
        templateUrl: 'views/porcentaje.html',
        controller: 'ctrl_porcentaje_cumplimiento'
      })
      .when('/reg_area', {
        templateUrl: 'views/reg_area.html',
        controller: 'ctrl_generic_datos'
      }).when('/reg_grupos', {
        templateUrl: 'views/reg_grupos.html',
        controller: 'ctrl_generic_datos'
      })
      .when('/reg_consecion', {
        templateUrl: 'views/reg_concecion.html',
        controller: 'ctrl_generic_datos'
      })
      .when('/reg_consecionario', {
        templateUrl: 'views/reg_concecionario.html',
        controller: 'ctrl_generic_datos'
      })
      .when('/reg_localidad', {
        templateUrl: 'views/reg_localidad.html',
        controller: 'ctrl_generic_datos'
      })
      .when('/reg_evidencia', {
        templateUrl: 'views/reg_evidencia.html',
        controller: 'ctrl_reg_evidencia'
      })
      .when('/reg_servicio', {
        templateUrl: 'views/reg_servicio.html',
        controller: 'ctrl_generic_datos'
      })
       .when('/reportes_fijos', {
        templateUrl: 'views/reportes_fijos.html',
        controller: 'ctrl_reportes_fijos'
      }).when('/filtrogpo', {
        templateUrl: 'views/filtrogpo.html',
        controller: 'ctrl_filtrogpo'
      })
      .otherwise({
        redirectTo: '/actualizacion_obligaciones2'
      });
  });
