/* 
 * @Author: yomero
 * @Date:   2014-11-10 12:46:16
 * @Last Modified by:   yomero
 * @Last Modified time: 2014-11-19 09:33:27
 */
'use strict';
miApp.controller('ctrl_resultados', function($scope, $rootScope, $timeout, $location, JSONDATA) {

    $scope.viewTotal = true;

    $scope.verTodos = function() {
        $scope.viewTotal = ($scope.viewTotal) ? false : true;
        if ($scope.viewTotal) $scope.gruposGenerados = {};
    }

    $scope.regresar = function() {
        $location.url("/informes");
    }

    $scope.filtros = JSONDATA.filtros;

    


    if (!$rootScope.genInfo || !$rootScope.genInfo.rowFilter) {
        $scope.regresar();
    } else {
        if ($rootScope.genInfo.rowFilter.length < 0) {
            $scope.regresar();
        }
    }


    $scope.elimina_grupo = function(grupo) {
        var index = $scope.agruparpor.indexOf(grupo);
        if (index > -1) $scope.agruparpor.splice(index, 1);
    }


    $scope.agruparpor = [];
    $scope.agregaGrupo = function(grupo) {
        var tmpArray = [];
        var grupo = {};
        grupo.orden = $scope.agruparpor.length;
        grupo.label = $scope.sel_grupo.label;
        grupo.value = $scope.sel_grupo.value;
        $scope.sel_option = "";
        _.each($scope.agruparpor, function(g) {
            tmpArray.push(g.value);
        })
        var contiene = _.contains(tmpArray, grupo.value);
        if (!contiene)
            $scope.agruparpor.push(grupo);
        console.info("Agrupar por",$scope.agruparpor);
    }

    $scope.borragrupo = function(grupo) {

    }



    var obtenerFilas = function(tipo, objetos) {
        var arregloRows = [];
        _.each(objetos, function(obligacion) {
            var datoEvaluar = "";
            switch (tipo) {
                case "nombre":
                case "servicio":
                case "grupo":
                case "status":
                    datoEvaluar = obligacion[tipo];
                    break;
                case "consecion":
                case "consecionario":
                case "region":
                    datoEvaluar = obligacion[tipo].nombre;
                    break;

                default:
                    break;
            }

            var valor = _.contains(arregloRows, datoEvaluar)
            if (!valor)
                arregloRows.push(datoEvaluar);

        });

        return arregloRows;
    }

    var idData = 0;
    var obtenerGrupos = function(tipo, filas, objetos) {
        idData++;
        var ArregloGrupos = [];
        _.each(filas, function(grupo, index) {
            var JSONDATAFinder = {};
            switch (tipo) {
                case "nombre":
                case "servicio":
                case "grupo":
                case "status":
                    JSONDATAFinder[tipo] = grupo;
                    break;
                case "consecion":
                case "consecionario":
                case "region":
                    JSONDATAFinder[tipo] = {};
                    JSONDATAFinder[tipo].nombre = grupo;
                    break;

                default:
                    break;
            }


            var datos = _.where(objetos, JSONDATAFinder);
            var titulo = grupo;
            var grupoGenerado = {
                titulo: titulo,
                datos: datos,
                id:idData
            }
            ArregloGrupos.push(grupoGenerado);
        })
        return ArregloGrupos;
    }


    function transforma_status(status){
        switch(status){
            case "A":
                return "Alerta"
                break;
            case "N":
                return "En tiempo"
                break;
            case "V":
                return "Vencido"
                break;

            default:
                return status;
                break;
        }
    }


    var posicionDatoPintar = 1;
    var pintaSubGrupos = function(grupopadre) {
        var contenedor = $("#wrapper_gpos");
        var desfase = 15;


        if(grupopadre.posicion)
            console.info("Dato posicion",grupopadre.posicion);
        console.info("Dato titulo",grupopadre.titulo);
        _.each(grupopadre, function(gpo,indicegpo) {
            console.info("***************************PINTO UNA TABLA CON EL TITULO::"+gpo.posicion+"************************************")
            if(typeof gpo.posicion !="undefined")
                posicionDatoPintar = gpo.posicion;
            else 
                gpo.posicion = posicionDatoPintar+2;
            contenedor.append("<h"+gpo.posicion+" style='margin-left:"+gpo.posicion*desfase+"px'> <span style='font-weight: bolder;color:red;opacity:.5'>&#10149; </span>"+transforma_status(gpo.titulo)+"</h"+gpo.posicion+">")
            if (gpo.subgrupos) {
                pintaSubGrupos(gpo.subgrupos);
            } else {
                var hash=gpo.id+moment();

                contenedor.append("<div id='wrap_dato"+hash+"'></div>")
                generaTablaConDatos("wrap_dato"+hash,gpo.datos);
                console.info("*******************************FIN DE DATOS***************************")
            }
        })
    }


    var generaTablaConDatos = function(contenedor,misdatos){
            var content = $("#"+contenedor);
            _.each(misdatos,function(dato){
                content.append("<div style='background:#ccc;padding:10px 0 0 10px;'>Obligacion:<b>"+dato.nombre+"</b></div>")
                content.append("<div style='background:#efefef;padding:10px;'>Porcentaje de cumplimiento:<b> "+(Math.random()*100).toFixed(2)+"%</b></div><br/>")
              
            })

        
        /*_.each(misdatos, function(data) {
                    var html = "";
                    var contenedor = $("#table_" + indice);
                    console.info("DATOS INNER", data);
                    html += "<tr>";
                    _.each($rootScope.genInfo.rowFilter, function(row) {
                        //console.info("BUSCO::",row.value);
                        switch (row.value) {
                            case "nombre":
                            case "servicio":
                            case "grupo":
                                var valor = (typeof data[row.value] == "undefined") ? "--Sin espeficar--" : data[row.value];
                                break;
                            case "consecion":
                            case "consecionario":
                            case "region":
                                var valor = (typeof data[row.value] == "undefined") ? "--Sin espeficar--" : data[row.value].nombre;
                                break;
                            case "status":
                                var valor = (typeof data[row.value] == "undefined") ? "--Sin espeficar--" : data[row.value];
                                if (valor == "A") valor = "Alerta";
                                if (valor == "N") valor = "En tiempo";
                                if (valor == "V") valor = "Vencido";

                                break;

                            default:
                                break;
                        }
                        html += "<td title='" + valor + "' style='white-space: nowrap;overflow: hidden;text-overflow: ellipsis;'>";
                        html += valor;
                        html += "</td>";

                    })
                    html += "</tr>";
                    contenedor.append(html);
                    html = "";



        })*/
    }


    var filtrosSelected = [];
    function generateFiltros(){
        filtrosSelected = [];
        _.each($scope.agruparpor, function(gpo){
            filtrosSelected.push(gpo.value);
        })

        return filtrosSelected;
    }

    var indexFiltro = 0;
    $scope.genTestData = function(){

        $("#wrapper_gpos").html("");
        generateFiltros();
        indexFiltro = 0;
        posicionDatoPintar = 2;
        console.info("GEN FILTROS::",filtrosSelected);
        if (filtrosSelected.length>0)
            $scope.genGrupoData(indexFiltro);

    }


    $scope.genGrupoData = function(filtro,objetos) {
        if(typeof objetos =="undefined"){

            objetos = $scope.obligaciones;
            var gruposFiltrados = [];
            var filas = obtenerFilas(filtrosSelected[filtro], objetos);
            if (filas.length > 0)
            var gpoData = obtenerGrupos(filtrosSelected[filtro], filas, objetos);
            if(typeof filtrosSelected[indexFiltro+1] != "undefined")
                $scope.genGrupoData(indexFiltro+1,gpoData);

        }else{
            console.info("OBJETOS DATOS::",objetos.length);
            indexFiltro +=1;
            _.each(objetos,function(grupo) {
                var subgrupFilas = obtenerFilas(filtrosSelected[indexFiltro], grupo.datos);
                var subgroupDatos = obtenerGrupos(filtrosSelected[indexFiltro], subgrupFilas, grupo.datos);
                delete grupo.datos;
                grupo.subgrupos = subgroupDatos;
                grupo.posicion = indexFiltro;
                console.info("Grupo en data::",grupo);
                if(typeof filtrosSelected[indexFiltro+1] != "undefined")
                $scope.genGrupoData(indexFiltro+1,grupo.subgrupos);
            })
        }
        
        $timeout(function(){
            pintaSubGrupos(gpoData);
        },1000)
        
    }


    $scope.genera_grupo = function(item) {
        $scope.viewTotal = false;
        console.info("ITEM", item);
        var JSONDATAGroup = {
            tipo: item.value,
            grupos: [],
            gruposGenerados: []
        };
        JSONDATAGroup.gruposGenerados = obtieneFilas("consecion");

        _.each(JSONDATAGroup.grupos, function(grupo, index) {
            var JSONDATAFinder = {};
            JSONDATAFinder[JSONDATAGroup.tipo] = {};
            JSONDATAFinder[JSONDATAGroup.tipo].nombre = grupo;
            var datos = _.where($scope.obligaciones, JSONDATAFinder);
            var titulo = grupo;
            var grupoGenerado = {
                titulo: titulo,
                datos: datos
            }
            JSONDATAGroup.gruposGenerados.push(grupoGenerado);
        })

        $scope.gruposGenerados = JSONDATAGroup.gruposGenerados;

        $timeout(function() {
            //table_titulo
            //

            _.each($scope.gruposGenerados, function(grupogen, indice) {

                console.info("GRUPO GEN", grupogen);
                console.info("GRUPO INDICE", indice);

                _.each(grupogen.datos, function(data) {
                    var html = "";
                    var contenedor = $("#table_" + indice);
                    console.info("DATOS INNER", data);
                    html += "<tr>";
                    _.each($rootScope.genInfo.rowFilter, function(row) {
                        //console.info("BUSCO::",row.value);
                        switch (row.value) {
                            case "nombre":
                            case "servicio":
                            case "grupo":
                                var valor = (typeof data[row.value] == "undefined") ? "--Sin espeficar--" : data[row.value];
                                break;
                            case "consecion":
                            case "consecionario":
                            case "region":
                                var valor = (typeof data[row.value] == "undefined") ? "--Sin espeficar--" : data[row.value].nombre;
                                break;
                            case "status":
                                var valor = (typeof data[row.value] == "undefined") ? "--Sin espeficar--" : data[row.value];
                                if (valor == "A") valor = "Alerta";
                                if (valor == "N") valor = "En tiempo";
                                if (valor == "V") valor = "Vencido";

                                break;

                            default:
                                break;
                        }
                        html += "<td title='" + valor + "' style='white-space: nowrap;overflow: hidden;text-overflow: ellipsis;'>";
                        html += valor;
                        html += "</td>";

                    })
                    html += "</tr>";
                    contenedor.append(html);
                    html = "";



                })


            })
        }, 500);



    }

    $scope.printpage = function() {
        window.print();
        return false;
    }

    $scope.obligaciones = JSONDATA.getObligacionesParsed();
    console.info("OBLIGACIONES::", $scope.obligaciones)


    //Tijuana Cable Más

    if ($rootScope.genInfo && $rootScope.genInfo.configfiltros && $rootScope.genInfo.configfiltros.length > 0) {
        console.info("TENGO FILTROS", $rootScope.genInfo.configfiltros);
        var JSONDATAFinder = {};
        _.each($rootScope.genInfo.configfiltros, function(item) {
            var toswitch = item.value
            switch (toswitch) {
                case "nombre":
                case "servicio":
                case "grupo":
                    JSONDATAFinder[toswitch] = item.valuedata;
                    break;
                case "consecion":
                case "consecionario":
                case "region":
                    JSONDATAFinder[toswitch] = {};
                    JSONDATAFinder[toswitch].nombre = item.valuedata;
                    break;
                case "status":
                    var valor = item.valuedata.toLowerCase();
                    if (valor == "alerta") valor = "A";
                    if (valor == "en tiempo") valor = "N";
                    if (valor == "vencido") valor = "V";
                    JSONDATAFinder[toswitch] = valor;
                    break;
                default:
                    break;
            }
            console.info("JSONDATA FINDER", JSONDATAFinder);
            $scope.obligaciones = _.where($scope.obligaciones, JSONDATAFinder);

        });

    }


    var contenedor = $("#content_data");
    var html = "";

    $timeout(function() {
        var html = "";
        _.each($scope.obligaciones, function(data) {
            html += "<tr>";
            _.each($rootScope.genInfo.rowFilter, function(row) {
                //console.info("BUSCO::",row.value);
                switch (row.value) {
                    case "nombre":
                    case "servicio":
                    case "grupo":
                        var valor = (typeof data[row.value] == "undefined") ? "--Sin espeficar--" : data[row.value];
                        break;
                    case "consecion":
                    case "consecionario":
                    case "region":
                        var valor = (typeof data[row.value] == "undefined") ? "--Sin espeficar--" : data[row.value].nombre;
                        break;
                    case "status":
                        var valor = (typeof data[row.value] == "undefined") ? "--Sin espeficar--" : data[row.value];
                        if (valor == "A") valor = "Alerta";
                        if (valor == "N") valor = "En tiempo";
                        if (valor == "V") valor = "Vencido";

                        break;

                    default:
                        break;
                }
                html += "<td title='" + valor + "' style='white-space: nowrap;overflow: hidden;text-overflow: ellipsis;'>";
                html += valor;
                html += "</td>";

            })
            console.info("html::", html);
            html += "</tr>";
            contenedor.append(html);
            html = "";
        })


    }, 1000);








});


miApp.controller('ctrl_informes', function($scope, $rootScope, $timeout, $location, JSONDATA) {



    $scope.filtros = JSONDATA.filtros;

    if (!$rootScope.genInfo) $rootScope.genInfo = {};
    if (!$rootScope.genInfo.configfiltros) $rootScope.genInfo.configfiltros = [];
    if ($rootScope.genInfo.rowFilter) delete $rootScope.genInfo.rowFilter;

    $scope.addFiltro = function() {
        if ($scope.sel_option && $scope.sel_option != "") {
            var filtro = {};
            filtro.id = $rootScope.genInfo.configfiltros.length;
            filtro.label = $scope.sel_option.label;
            filtro.value = $scope.sel_option.value;
            $scope.sel_option = "";
            $rootScope.genInfo.configfiltros.push(filtro);
            console.info("FILTROS::", $rootScope.genInfo.configfiltros);
        }

    }

    //Tijuana Cable Más

    $scope.addValueFilter = function(item, component) {
        item.valuedata = component.nada;
    }


    $scope.goResult = function() {
        $location.url("/resultados");
    }

    $scope.remove_filter = function(objeto) {

        bootbox.confirm("¿Realmente desea elminiar este elemento de la lista?", function(result) {
            if (result) {
                $timeout(function() {
                    var indice = $rootScope.genInfo.configfiltros.indexOf(objeto);
                    if (indice > -1) {
                        $rootScope.genInfo.configfiltros.splice(indice, 1)
                    }
                }, 500);


            }

        })



    }



    $timeout(function(){
        $(".checkbox").trigger("click");
    },500)



    $scope.addrow_filter = function(row) {
        console.info("entro",row);
        if (!$rootScope.genInfo.rowFilter) $rootScope.genInfo.rowFilter = [];
        var indice = _.findIndex($rootScope.genInfo.rowFilter,row);
        if (indice > -1) {
            $rootScope.genInfo.rowFilter.splice(indice, 1)
        } else {
            $rootScope.genInfo.rowFilter.push(row)
        }
        console.info("ROW", $rootScope.genInfo.rowFilter);
    }

});


miApp.controller('ctrl_reportes_fijos', function($scope, $rootScope, JSONDATA) {
    $scope.consecionarios = JSONDATA.consecionarios;

    $scope.obligaciones = JSONDATA.getObligacionesParsed();

    $scope.muestra_registros = function(tipo) {
        _.each($scope.obligaciones, function(obligacion) {
            obligacion.view = false;
            if (tipo == "AM") {
                obligacion.view = true;
            } else {
                if (obligacion.status == tipo) {
                    obligacion.view = "true";
                }
            }
        })

    }



});


miApp.controller('ctrl_porcentaje_cumplimiento', function($scope, $timeout, JSONDATA) {

    $scope.obligaciones = JSONDATA.getObligacionesParsed();


    $timeout(function() {
        _.each($scope.obligaciones, function(obligacion) {
            //$scope.getTiempo(obligacion.historico[0].fecharegistro,obligacion.historico[0].fechavencimiento);
            _.each(obligacion.historico, function(h) {
                h.diferencia = $scope.getTiempo(h.fecharegistro, h.fechavencimiento);
            })
        });

    }, 500);

    $timeout(function() {
        _.each($scope.obligaciones, function(obligacion) {

            //$scope.getTiempo(obligacion.historico[0].fecharegistro,obligacion.historico[0].fechavencimiento);
            obligacion.tiempoprevio = $scope.calculatiempoPrevio(obligacion.historico);
            obligacion.porcentaje = $scope.calculaPorcentaje(obligacion.historico);
            obligacion.tiempomedio = $scope.calculaMedia(obligacion.historico);
            var dif = obligacion.tiempomedio;
            var cumplimiento = "";
            if (dif > 30) cumplimiento = "mp"
            if (dif > 0) cumplimiento = "mo"
            if (dif < 15) cumplimiento = "qd"
            if (dif < 5) cumplimiento = "sn"
            obligacion.cumplimiento = cumplimiento;

        });
    }, 1000);

    $scope.filtro_reg = function(tipo) {
        _.each($scope.obligaciones, function(item) {
            item.visible = false;
            var ic = item.cumplimiento;
            if (tipo == ic) {
                item.visible = true;
            }
            if (tipo != "mp") {
                if (tipo == "mo" && ic == "qd" || ic == "sn") {
                    item.visible = true;
                }
            }


        })
    }

    $scope.calculatiempoPrevio = function(historico) {
        var valor = 0;
        var tamano = 0;
        _.each(historico, function(h) {
            if (h.diferencia > 0 && h.diferencia != "Aun no registra evidencia") {
                valor += h.diferencia;
                tamano += 1;
            }
        })
        //console.info("HISTORICO::",historico);
        //console.info("TIEMPO PREVIO::",valor/tamano)
        return (valor / tamano).toFixed(2);
    }

    $scope.dataFixed = function(dato){
        return dato.toFixed(2);
    }

    $scope.calculaPorcentaje = function(historico) {
        var valor = 0;
        _.each(historico, function(h) {
            if (h.diferencia > 0 && h.diferencia != "Aun no registra evidencia") {
                valor += 1;
            }
        });

        return (valor / historico.length) * 100
    }

    $scope.calculaMedia = function(historico) {
        var valor = 0;
        _.each(historico, function(h) {
            if (h.diferencia != "Aun no registra evidencia")
                valor += h.diferencia;
        });
        //console.info(historico);
        //console.info((valor/historico.length).toFixed(2));
        return (valor / historico.length).toFixed(2);
    }


    $scope.getTiempo = function(fechainicio, fechafin) {
        var a = moment(fechainicio, "DD-MM-YYYY");
        var b = moment(fechafin, "DD-MM-YYYY");

        if (fechainicio != "") {
            console.info(b.diff(a, 'days'));
            return b.diff(a, 'days');
        } else {
            console.info("Aun no registra evidencia");
            return "Aun no registra evidencia";
        }
    }

    /// INICIALIZO FUNCIONES
    $timeout(function() {
        $scope.filtro_reg('sn');
    }, 1000);

});