/* 
 * @Author: yomero
 * @Date:   2014-11-06 15:37:01
 * @Last Modified by:   yomero
 * @Last Modified time: 2014-12-02 18:46:54
 */
'use strict';

miApp.controller('ctrl_obligaciones', function($scope, JSONDATA) {

    /*var entries = conexionJSONDATA.query(function() {
    console.log(entries);
  	});*/


    $scope.areas = JSONDATA.areas;

    /*conexionJSONDATA.obtieneJSONDATA({},function(data){
    	console.info("OK DATA")
    },function(error){
    	console.info("OK ERROR");
    });*/

});

miApp.controller('ctrl_obligaciones2', function($scope,$timeout, JSONDATA, $location, $rootScope) {
    
    
    $scope.obligaciones = JSONDATA.actualizaobligacion;

    
    $rootScope.obligacionesRegistradas = [];
    if(!$rootScope.evidenciasGuardadas) $rootScope.evidenciasGuardadas = [];
    else{

    	console.info("EN ROOT AL OBTENER::",$rootScope.evidenciasGuardadas);
    	_.each($rootScope.evidenciasGuardadas,function(item){
    		
    		var obj = _.where($scope.obligaciones, {id:item.obligacion.id});
    		var indice = _.indexOf($scope.obligaciones,obj[0]);
	    	if(indice>-1){
	    		$scope.obligaciones.splice(indice, 1);
	    	}  

		})
    }
    
    



    $scope.agregaObligacion = function(obligacion) {
        var indice = $rootScope.obligacionesRegistradas.indexOf(obligacion);
        if (indice > -1) {
            $rootScope.obligacionesRegistradas.splice(indice, 1);
        } else {
            $rootScope.obligacionesRegistradas.push(obligacion);
        }
    }

    $scope.goToRegistraEvidencia = function() {
        $location.url("/reg_evidencia");
    }
});

miApp.controller('ctrl_reg_evidencia', function($scope, $location, $rootScope, $timeout) {
    
    //valido si previamente seleccione obligaciones si no lo redirecciono
    if (!$rootScope.obligacionesRegistradas || $rootScope.obligacionesRegistradas.length < 1) {
        //sinObligaciones();
    } else {
        $scope.obligaciones = $rootScope.obligacionesRegistradas;
        delete $rootScope.obligacionesRegistradas;
    }


    $scope.borraObligacion = function(obligacion) {
        bootbox.confirm("¿Realmente desea elminiar este elemento de la lista?", function(result) {

            if (result) {
                console.info("OK RESULT");
                var indice = $scope.obligaciones.indexOf(obligacion);
                if (indice > -1) {
                    $scope.obligaciones.splice(indice, 1);
                    $timeout(function() {
                        muestraAlerta()
                    }, 1000);
                }
            }
            if ($scope.obligaciones.length < 1) sinObligaciones()
        });


    }




    $scope.guarda_evidencias = function() {

        $.each($(".inputfiledata"), function(i) {
            var id = $(this).attr("idobligacion");
            var obligacion = $(this).attr("obligacion");
            var comentario = $("#comentario"+id).val();
            var input = document.getElementById("file" + id);
            var base = readImage(input, $scope, function() {
                $("#imagen" + id).attr("src", $scope.img);
                $scope.guarda_evidencia(obligacion,$scope.img,comentario);
            	
            });

        })


		$timeout(function(){
    		console.info("EN ROOT AL GUARDAR:",$rootScope.evidenciasGuardadas);
        	$location.url("/actualizacion_obligaciones2");
		},1500);

    }


$scope.guarda_evidencia = function(obligacion,imagen,comentario){

		var evidencia = {};
    	obligacion = JSONDATA.parse(obligacion);
    	evidencia.obligacion = obligacion;
    	evidencia.imagen = imagen;
    	evidencia.comentario = comentario;
    	if(evidencia.imagen !="")
    		$rootScope.evidenciasGuardadas.push(evidencia);
    }

    function muestraAlerta() {
        $("#alerta").show("slow", function() {
            $timeout(function() {
                $(".alert-success").hide('slow');
            }, 2000);
        });

    }

    function sinObligaciones() {
        bootbox.alert("Es necesario tener obligaciones seleccionadas para registrar una evidencia")
        $location.url("/actualizacion_obligaciones2");
    }

})