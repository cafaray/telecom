miApp.controller('ctrl_generic_datos', function($scope, JSONDATA) {

    $scope.obligaciones = JSONDATA.getObligacionesParsed();
    $scope.grupos = JSONDATA.grupos;
    $scope.conseciones = JSONDATA.conseciones;
    $scope.consecionarios = JSONDATA.consecionarios;
    $scope.localidades = JSONDATA.localidades;
    $scope.periodicidades = JSONDATA.periodicidades;
    $scope.aplica_a = JSONDATA.aplica_a;
    $scope.areas = JSONDATA.areas;

});


miApp.controller('ctrl_filtrogpo', function($scope, JSONDATA) {

    $scope.obligaciones = JSONDATA.getObligacionesParsed();
    $scope.grupos = JSONDATA.grupos;
    $scope.conseciones = JSONDATA.conseciones;
    $scope.consecionarios = JSONDATA.consecionarios;
    $scope.localidades = JSONDATA.localidades;
    $scope.periodicidades = JSONDATA.periodicidades;
    $scope.aplica_a = JSONDATA.aplica_a;
    $scope.areas = JSONDATA.areas;


    function getporciento(){
    	return (Math.random()*100).toFixed(2);
    }



    $scope.obligacionData=[{
    	localidad:"Durango",
    	consecion:"Durango Cablevisión",
    	grupo:"Cablevisión",
    	maxfecha:30,
    	minfecha:1,
    	porciento:getporciento()
    },{
    	localidad:"Durango",
    	consecion:"Durango Bestel",
    	grupo:"Bestel",
    	maxfecha:30,
    	minfecha:1,
    	porciento:getporciento()
    },{
    	localidad:"Durango",
    	consecion:"Durango Cable más",
    	grupo:"Cablemás",
    	maxfecha:10,
    	minfecha:5,
    	porciento:getporciento()
    },{
    	localidad:"Guerrero",
    	consecion:"Guerrero Cablevisión",
    	grupo:"Cablevisión",
    	maxfecha:70,
    	minfecha:10,
    	porciento:getporciento()
    },
    {
    	localidad:"Guerrero",
    	consecion:"Iguala Bestel",
    	grupo:"Bestel",
    	maxfecha:70,
    	minfecha:10,
    	porciento:getporciento()
    },
    {
    	localidad:"Oaxaca",
    	consecion:"Oaxaca Cablecom",
    	grupo:"Cablecom",
    	maxfecha:70,
    	minfecha:10,
    	porciento:getporciento()
    },{
    	localidad:"Oaxaca",
    	consecion:"Puerto escondido Cablevisión",
    	grupo:"Cablevisión",
    	maxfecha:70,
    	minfecha:10,
    	porciento:getporciento()
    },{
    	localidad:"Pachuca",
    	consecion:"Pachuca Bestel",
    	grupo:"Bestel",
    	maxfecha:40,
    	minfecha:14,
    	porciento:getporciento()
    },
    {
    	localidad:"Pachuca",
    	consecion:"Hidalgo Bestel",
    	grupo:"Bestel",
    	maxfecha:40,
    	minfecha:14,
    	porciento:getporciento()
    },{
    	localidad:"Pachuca",
    	consecion:"Apan Bestel",
    	grupo:"Bestel",
    	maxfecha:40,
    	minfecha:14,
    	porciento:getporciento()
    },
    ];


    $scope.cleanData = function(datos,soap){
    	var arreglo = [];
    	var arreglo = _.pluck(datos,soap);
    	return _.uniq(arreglo);
    }

    
    var OaxConsecion = ["Oaxaca Cablecom","Puerto escondido Cablevisión"];
    var OaxGrupo = ["Cablecom","Cablevisión"];
    var generalOaxaca = [{
    	localidad:"Oaxaca",
    	consecion:"Oaxaca Cablecom",
    	grupo:"Cablecom",
    	maxfecha:70,
    	minfecha:10,
    	porciento:getporciento()
    },{
    	localidad:"Oaxaca",
    	consecion:"Puerto escondido Cablevisión",
    	grupo:"Cablevisión",
    	maxfecha:70,
    	minfecha:10,
    	porciento:getporciento()
    }]
    var OaxLocalidadB = false;


    var GuerreroB = false;
    var DurangoB = false;
    var GuerreroLocalidad = ["Guerrero","Durango"];
    var GuerreroGrupo = ["Cablevisión","Bestel"];
    var generalGuerrero = [{
    	localidad:"Durango",
    	consecion:"Durango Cablevisión",
    	grupo:"Cablevisión",
    	maxfecha:30,
    	minfecha:1,
    	porciento:getporciento()
    },{
    	localidad:"Durango",
    	consecion:"Durango Bestel",
    	grupo:"Bestel",
    	maxfecha:30,
    	minfecha:1,
    	porciento:getporciento()
    },{
    	localidad:"Durango",
    	consecion:"Durango Cable más",
    	grupo:"Cablemás",
    	maxfecha:10,
    	minfecha:5,
    	porciento:getporciento()
    },{
    	localidad:"Guerrero",
    	consecion:"Guerrero Cablevisión",
    	grupo:"Cablevisión",
    	maxfecha:70,
    	minfecha:10,
    	porciento:getporciento()
    },
    {
    	localidad:"Guerrero",
    	consecion:"Iguala Bestel",
    	grupo:"Cablevisión",
    	maxfecha:70,
    	minfecha:10,
    	porciento:getporciento()
    }]

    $scope.changeGuerrero = function(data){

    	GuerreroB = (data=="Guerrero Cablevisión")?true:false;

    	if(!GuerreroB)
    		{
    			$scope.refresData();
    			return;
    		}else{
    			$scope.obligacionesDataFilter = generalGuerrero;
    			$scope.grupos = GuerreroGrupo;
    			$scope.localidades = GuerreroLocalidad;    			
    	}
    }

    $scope.changeOaxaca = function(data){

    	   	OaxLocalidadB = (data=="Oaxaca")?true:false;
    	   	if(!OaxLocalidadB)
    		{
    			$scope.refresData();
    			return;
    		}else{
    			$scope.obligacionesDataFilter = generalOaxaca;
    			$scope.grupos = OaxGrupo;
    			$scope.conseciones = OaxConsecion;    			
    	}

    }






    var CablevisionB = false;
    var CablevisionLocalidad = ["Durango","Guerrero","Oaxaca"]
    var CablevisionConsecion = ["Durango Cablevisión","Guerrero Cablevisión","Puerto escondido Cablevisión"]
    var CablevisionGeneral = [{
    	localidad:"Durango",
    	consecion:"Durango Cablevisión",
    	grupo:"Cablevisión",
    	maxfecha:30,
    	minfecha:1,
    	porciento:getporciento()
    },{
    	localidad:"Guerrero",
    	consecion:"Guerrero Cablevisión",
    	grupo:"Cablevisión",
    	maxfecha:70,
    	minfecha:10,
    	porciento:getporciento()
    },{
    	localidad:"Guerrero",
    	consecion:"Iguala Bestel",
    	grupo:"Cablevisión",
    	maxfecha:70,
    	minfecha:10,
    	porciento:getporciento()
    },{
    	localidad:"Oaxaca",
    	consecion:"Puerto escondido Cablevisión",
    	grupo:"Cablevisión",
    	maxfecha:70,
    	minfecha:10,
    	porciento:getporciento()
    }]

     $scope.changeCablevision = function(data){

    	   	CablevisionB = (data=="Cablevisión")?true:false;
    	   	if(!CablevisionB)
    		{
    			$scope.refresData();
    			return;
    		}else{
    			$scope.obligacionesDataFilter = CablevisionGeneral;
    			$scope.localidades = CablevisionLocalidad;
    			$scope.conseciones = CablevisionConsecion;    			
    	}

    }
    


    $scope.refresData = function(tipo){
    	 		$scope.obligacionesDataFilter = $scope.obligacionData;
    			$scope.grupos = $scope.cleanData($scope.obligacionesDataFilter,"grupo");
    			$scope.localidades = $scope.cleanData($scope.obligacionesDataFilter,"localidad");
    			$scope.conseciones = $scope.cleanData($scope.obligacionesDataFilter,"consecion");

    	
    }

    $scope.refresData();

    $scope.getDataFiltered = function(filtro){
    	$scope.obligacionesDataFilter = $scope.obligacionData;
    	$scope.obligacionesDataFilter = _.where($scope.obligacionesDataFilter,filtro);
    }

    $scope.filtraXConsecion= function(conse){
    	var JsonData = {consecion:conse};
    	$scope.getDataFiltered(JsonData);
    	$scope.refresData("g");
    	$scope.refresData("l");
    }
    


});

