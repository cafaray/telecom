'use strict';

/**
 * @ngdoc function
 * @name televisaApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the televisaApp
 */
miApp.controller('AboutCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
